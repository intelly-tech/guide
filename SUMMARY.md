# Table of contents

* [Introduction](README.md)

## Intelly

* [Platform](intelly/platform/README.md)
  * [Exchange](intelly/platform/exchange.md)
  * [Reserve](intelly/platform/reserve.md)
* [Tokens](intelly/tokens/README.md)
  * [Token (INTL)](intelly/tokens/token-intl.md)
  * [Investment (INFT)](intelly/tokens/investment-inft.md)
* [Contracts](intelly/contracts/README.md)
  * [Access](fundamentals/intelly-contracts/tools/access.md)
  * [Oracle](fundamentals/intelly-contracts/tools/oracle.md)
  * [Swap](fundamentals/intelly-contracts/tools/swap.md)
  * [Platform](fundamentals/intelly-contracts/tools/platform.md)

## Fundamentals

* [Crypto Wallets](fundamentals/crypto-wallets/README.md)
  * [MetaMask](fundamentals/crypto-wallets/metamask/README.md)
    * [Install](fundamentals/crypto-wallets/metamask/install.md)
    * [Add Network](fundamentals/crypto-wallets/metamask/add-network.md)
    * [Import Tokens](fundamentals/crypto-wallets/metamask/import-tokens.md)
    * [Import NFT's](fundamentals/crypto-wallets/metamask/import-nfts.md)
  * [WalletConnect](fundamentals/crypto-wallets/walletconnect.md)
* [Binance Smart Chain](fundamentals/binance-smart-chain/README.md)
  * [Binance Support](fundamentals/binance-smart-chain/binance-support.md)
  * [Withdraw](fundamentals/binance-smart-chain/withdraw.md)
* [Intelly Platform](fundamentals/intelly-platform/README.md)
  * [Connect](fundamentals/intelly-platform/connect.md)
  * [Verify](fundamentals/intelly-platform/verify.md)
  * [Invest](fundamentals/intelly-platform/invest.md)
  * [Exchange](fundamentals/intelly-platform/exchange.md)

## Use Cases

* [For Investors](use-cases/for-investors.md)
* [For Real Estate Brokers](use-cases/for-real-estate-brokers.md)
* [For Real Estate Developers](use-cases/for-real-estate-developers.md)
* [For Software Developers](use-cases/for-software-developers.md)
