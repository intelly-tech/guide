---
description: About BSC
---

# Binance Smart Chain

In 2017, Binance and BNB were born. Three years later, [Binance Smart Chain](https://academy.binance.com/en/articles/an-introduction-to-binance-smart-chain-bsc) (BSC) was introduced to the world.  As Binance grew bigger and stronger, so did Binance Smart Chain.  BSC was [born](https://www.binance.com/en/blog/all/binance-smart-chain-launches-today-421499824684900933) in time for the DeFi revolution, as the public showed increased interest in alternative financial solutions and use cases powered by blockchain. Today, both Binance and BSC remain connected by BNB.

{% content-ref url="binance-support.md" %}
[binance-support.md](binance-support.md)
{% endcontent-ref %}

{% content-ref url="withdraw.md" %}
[withdraw.md](withdraw.md)
{% endcontent-ref %}
