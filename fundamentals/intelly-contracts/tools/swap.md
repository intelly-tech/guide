---
description: >-
  A Smart Contract for exchange Intelly Token (INTL) for Binance Stablecoin
  (BUSD) or vice versa.
---

# Swap

## Contract info

**Contract name:** Exchange

**Contract address:** 0xd3cE87b8981feF2fbb597D2c55Fc81eCf3BDA5F4

View [Oracle.sol](https://github.com/intelly-dev/contracts/blob/main/contracts/utils/Exchange.sol) on GitHub

View [Intelly: Oracle contract on BscScan](https://testnet.bscscan.com/address/0x94C21E6d308fbde6630da3608ca82e40DD16cFac) (Sample)



{% content-ref url="platform.md" %}
[platform.md](platform.md)
{% endcontent-ref %}
