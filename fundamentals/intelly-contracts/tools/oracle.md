---
description: >-
  A Smart Contract to get Intelly Token (INTL) price while Investing, Exchange
  and Transfer operations.
---

# Oracle

## Contract info

**Contract name:** Oracle

**Contract address:** 0x811BcBd3cDF3df65c78C0e834D92EA8A10305228

View [Intelly: Oracle contract on BscScan](https://testnet.bscscan.com/address/0x9388d33cf69976ae86eD0CeE30115a2b0631F22D) (Sample)



__

{% content-ref url="platform.md" %}
[platform.md](platform.md)
{% endcontent-ref %}
