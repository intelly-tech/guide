---
description: Here’s what we have to offer
---

# Intelly Platform

The Intelly platform is where investors can see all ongoing investments. The platform offers our community global investments from the whole spectrum of real estate. Investments are in tokenized form, ready to be invested in.

### Intelly Platform: Learn usage

{% content-ref url="connect.md" %}
[connect.md](connect.md)
{% endcontent-ref %}

{% content-ref url="verify.md" %}
[verify.md](verify.md)
{% endcontent-ref %}

{% content-ref url="invest.md" %}
[invest.md](invest.md)
{% endcontent-ref %}

{% content-ref url="exchange.md" %}
[exchange.md](exchange.md)
{% endcontent-ref %}
