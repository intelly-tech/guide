---
description: BEP1155 Fungible-Agnostic Tokens on Binance Smart Chain
---

# Investment (INFT)

{% hint style="info" %}
**Intelly: Investment** is based on **OpenZeppelin**'s **ERC1155**. Read the [OpenZeppelin documentation](https://docs.openzeppelin.com/contracts/4.x/erc1155) for detailed contract logic.
{% endhint %}

{% embed url="https://www.youtube.com/watch?v=KD1x-3V8qR8" %}

## Contract info

**Contract address:** There will be independent addresses for each project.&#x20;

You can check the investments on [Intelly Platform](https://app.intelly.tech/)



{% hint style="info" %}
_See details for **Intelly: Investment** sample contract on **fundamentals** section._
{% endhint %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}
