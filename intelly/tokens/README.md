---
description: BEP20 Fungible Tokens and BEP1155 Fungible-Agnostic Tokens
---

# Tokens

In this section you can find basic information about Intelly Tokens.

### Intelly Tokens: Check details&#x20;

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="investment-inft.md" %}
[investment-inft.md](investment-inft.md)
{% endcontent-ref %}

{% hint style="info" %}
For more detailed information, please check the [fundamentals ](broken-reference)section.
{% endhint %}
